--extra-index-url https://nexus.tiki-dsp.io/repository/tiki-pypi/simple
ai-common==1.0.0
torch==1.12.0
torchvision==0.13.0
numpy==1.21.6
