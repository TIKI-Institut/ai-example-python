import os

from ai_common.decorators import web_resource
from flask import request, send_from_directory


@web_resource('/<path:path>', None)
def server_static_resources(path):
    return send_from_directory('res', path)


@web_resource('/', None)
def index():
    return send_from_directory('res', 'index.html')


@web_resource('/welcome', None)
def welcome():
    return "<p>This is a welcome page</p>" \
           '<a href="./test_open">To open endpoint</a>'


@web_resource('/test_open', None)
def open_endpoint():
    return "This is an open endpoint from the main with no login required"


@web_resource('/resource_limits', None)
def resource_limits():
    cpu_limit: str = os.getenv("CPU_LIMIT")
    if cpu_limit is None:
        cpu_limit = "unavailable"

    memory_limit: str = os.getenv("MEMORY_LIMIT")
    if memory_limit is None:
        memory_limit = "unavailable"

    return "cpu limit: {cpu} m, memory limit: {memory} Mi".format(cpu=cpu_limit, memory=memory_limit)


@web_resource('/test_secured', [])
def secured_endpoint():
    return "This endpoint from main need a login"


@web_resource('/test_secured_with_roles', ["capability-1"])
def secured_endpoint_with_roles():
    name = request.values.get("name")
    if name is not None:
        return "This endpoint, %s, from main need a login and an assigned role" % name
    else:
        return "This endpoint, dear user, from main need a login and an assigned role"
