import pyspark
import os
from ai_common.decorators import job
import subprocess
import click

#hdfs_path = "hdfs:///tiki/tests/python/text.txt"
@job("hdfs-job", None, short_help='hdfs test')
@click.option('--hdfs_path', type=str,  help='Path where to save the text file.')
def main_function(hdfs_path):
    spark = pyspark.sql.SparkSession.builder \
        .config("spark.app.name", "pyspark_example") \
        .getOrCreate()
    sc = spark.sparkContext

    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    print("Local word count")
    rel_path = "text.txt"
    abs_file_path = os.path.join(script_dir, rel_path)
    file = open(abs_file_path,"r+")

    local_word_count = 0
    for word in file.read().split():
        local_word_count += 1


    print("Remove file {} from hdfs if exists".format(hdfs_path))
    subprocess.call(["hadoop", "fs", "-rm", "-r", "-f", hdfs_path])

    f = open(abs_file_path, 'r')
    x = f.readlines()
    f.close()
    print("Writing local file to DFS")
    fileRDD = sc.parallelize(x)
    fileRDD.saveAsTextFile(hdfs_path)

    print("Reading file from DFS and running Word Count")
    readFileRDD = sc.textFile(hdfs_path)

    dfs_word_count = readFileRDD \
        .flatMap(lambda x: x.split(" ")) \
        .flatMap(lambda x: x.split("\t")) \
        .filter(lambda x: x!="") \
        .map(lambda x: (x, 1)) \
        .count()

    spark.stop()

    if (local_word_count == dfs_word_count):
        print("Success! Local Word Count {} and DFS Word Count {} agree.".format(local_word_count, dfs_word_count))
    else:
        raise Exception("Failure! Local Word Count {} and DFS Word Count {} disagree.".format(local_word_count, dfs_word_count))

    return



