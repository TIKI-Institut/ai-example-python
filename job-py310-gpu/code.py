from ai_common.decorators import job
import click
import tensorflow as tf


@job("insecure-job", None)
def job2():
    device_list = tf.config.list_physical_devices('GPU')
    if(len(device_list)==0):
        raise Exception("No gpu found")
    click.echo(device_list)

    # train mnist classifier, see https://www.tensorflow.org/overview
    mnist = tf.keras.datasets.mnist

    (x_train, y_train),(x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0

    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(10, activation='softmax')
    ])

    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    model.fit(x_train, y_train, epochs=5)
    model.evaluate(x_test, y_test)
