@Library(value='tiki/jenkins@ZOLINFRA-21', changelog=false) _

pipeline {
    agent {
        kubernetes {
            defaultContainer 'spark3'
            yamlMergeStrategy merge()
            inheritFrom 'dsp-spark3 dsp-provision dsp-integration-test'
            yaml """
                apiVersion: v1
                kind: Pod
                metadata:
                spec:
                  containers:
                  - name: provisioner
                    image: docker.tiki-dsp.io/cicd-provisioner:${params.PROVISIONER_VERSION}
                """
        }
    }

    options {
        buildDiscarder(logRotator(daysToKeepStr: '14'))
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    parameters {
        string(name: 'KERNEL_VERSION', defaultValue: 'latest', description: 'ai-kernel version')
        string(name: 'PROVISIONER_VERSION', defaultValue: 'latest', description: 'cicd-provisioner version')
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

    environment {
        PRINCIPAL   = 'tiki-test'
        PROJECT     = 'ai-example-python'
        ENVIRONMENT = aiExampleEnvironment(env.BRANCH_NAME, params.KERNEL_VERSION, params.PROVISIONER_VERSION)
    }

    stages {
        stage('Prepare') {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS')
                echo "Starting pipeline for branch '${env.BRANCH_NAME}' with KERNEL_VERSION '${params.KERNEL_VERSION}' and PROVISIONER_VERSION '${params.PROVISIONER_VERSION}'"
            }
        }
        stage('Provisioner') {
            stages {
                stage('prepare') {
                    steps {
                        container('provisioner') {
                            script {
                                sh "mkdir -p /home/jenkins/backup"
                                generateCiMetaFile("${params.KERNEL_VERSION}", "${ENVIRONMENT}")
                            }
                        }
                    }
                }
            }
        }
        stage('decommission') {
            steps {
                container('provisioner') {
                    script {
                        try {
                            sh "ai-provisioner -p /opt/.provisioner decommission ${WORKSPACE}"
                        } catch (ex) {
                            echo 'Decommission failed, skipping...'
                        }
                    }
                }
            }
        }
        stage('setup project') {
            steps {
                container('provisioner') {
                    sh "ai-provisioner -p /opt/.provisioner setup ${WORKSPACE}"
                }
            }
        }
        stage('assign keycloak client roles') {
            options { skipDefaultCheckout() }
            steps {
                container('keycloak') {
                    sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.io/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                    sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT} --rolename dsp-deploy --rolename dsp-undeploy --rolename dsp-list --rolename dsp-decommission --rolename dsp-provision"
                }
            }
        }
        stage('provision') {
            steps {
                container('provisioner') {
                    sh "ai-provisioner -p /opt/.provisioner provision ${WORKSPACE}"
                }
            }
        }
        stage('assign keycloak client roles for secure job') {
            options { skipDefaultCheckout() }
            steps {
                container('keycloak') {
                    sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.io/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                    sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT}-job-py310-role-secured --rolename role"
                }
            }
        }
        stage('deploy') {
            steps {
                container('provisioner') {
                    sh "ai-provisioner -p /opt/.provisioner deploy ${WORKSPACE}"
                }
            }
        }
        stage('Check job status') {
            steps {
                container('kubectl') {
                    script {
                        checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-py310")
                        checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-py310-role-secured")
                        checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-py310-with-cli-args")
                        checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-py310-gpu")
                        checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-py310-spark3-kube-gpu")
                        checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-py310-spark3-kube-hdfs")
                        checkJobStatus(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", flavorName: "job-py310-torch-gpu")
                    }
                }
            }
        }
        stage('Check web services') {
            options { skipDefaultCheckout() }
            steps {
                container('keycloak') {
                    script {

                        ///////////////////////////////
                        // Check web-py310 Webservice
                        ///////////////////////////////

                        // get roles to use secure endpoints
                        sh "/opt/keycloak/bin/kcadm.sh config credentials --server https://auth.tiki-dsp.io/auth --realm ${PRINCIPAL} --user \$(cat /opt/kc/credentials.json | jq -r .username) --password \$(cat /opt/kc/credentials.json | jq -r .password)"
                        sh "/opt/keycloak/bin/kcadm.sh add-roles --uusername \$(cat /opt/kc/credentials.json | jq -r .username) --cclientid ${PROJECT}-${ENVIRONMENT}-web-py310 --rolename capability-1"
                        // get token for secure endpoint and save it in access_token.json
                        ACCESS_TOKEN = sh (
                            script: "set +x \n curl --location --request POST 'https://auth.tiki-dsp.io/auth/realms/${PRINCIPAL}/protocol/openid-connect/token' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'client_id=${PROJECT}-${ENVIRONMENT}' --data-urlencode 'grant_type=password' --data-urlencode username=\$(cat /opt/kc/credentials.json | jq -r .username) --data-urlencode password=\$(cat /opt/kc/credentials.json | jq -r .password) | jq -r .access_token |tr -d '\\n'",
                            returnStdout: true
                        ).trim()

                        // check open endpoint
                        checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.io/${PROJECT}/${ENVIRONMENT}/web-py310/test_open")

                        // check secure endpoints
                        checkWebStatus(request: "GET", url: "https://${PRINCIPAL}.tiki-dsp.io/${PROJECT}/${ENVIRONMENT}/web-py310/test_secured_with_roles", bearerToken: "${ACCESS_TOKEN}")
                    }
                }
            }
        }
		stage('check custom CA certs') {
			steps {
				container('kubectl') {
					script {
						checkCaCertInLinuxStore(principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", k8sResource: "deployment/web-py310", certCommonName: "test-certificate-for-examples")
					}
				}
			}
		}
        stage('Check DNS service') {
           steps {
               container('kubectl') {
                   script {
                       projectParams = [principal: "${PRINCIPAL}", projectName: "${PROJECT}", envName: "${ENVIRONMENT}", k8sResource: "deployment/web-py310"]
                       installNslookup(projectParams + [hostname: "tiki-institut.com"])
                       nslookup( projectParams + [hostname: "tiki-institut.com"])
                       nslookup( projectParams + [hostname: "intern.test-dns"])
                       nslookup( projectParams + [hostname: "jupyterhub.tiki-test.svc.cluster.local"])
                   }
               }
           }
        }
    }

    post {
        failure {
            doFailureSteps()
        }
        fixed {
            doFixedSteps()
        }
    }
}
