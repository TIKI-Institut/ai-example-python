import click
from ai_common.decorators import job


@job("role-secured-job", ['role'])
def job():
    click.echo("Hello World (from role-secured-job)")
