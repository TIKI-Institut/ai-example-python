import sys

from ai_common.logging import setup_logger, log_info
from ai_common.decorators import job
from ai_common.dw.client import DWClient


@job("insecure-job", None)
def job2():
    setup_logger("LOG_LEVEL")
    log_info("Hello World (from insecure-job)!!!")
    log_info(f"Python version: {sys.version}")


@job("workbenchclient-job", None)
def workbench_client_job():
    client = DWClient(service_url="https://workbench.tiki-test.tiki-dsp.io/tiki-test",
                      token_path="/dsp/auth/token")
    base_folder = "hdfs:/ai-example/dw/python-client-test"

    try:
        # fs
        fs_result = client.fs()
        log_info(f"fs result: {fs_result}")

        # download
        download_result = client.download(source=f"{base_folder}/src_download/download.txt")
        log_info(f"download result: {download_result}")

        # upload
        upload_file = "/tmp/upload_test_file.txt"
        open(upload_file, "w").close()
        upload_result = client.upload(to=f"{base_folder}/dst_upload/",
                                      file=upload_file,
                                      overwrite=True)
        log_info(f"upload result: {upload_result}")

        # copy
        copy_result = client.copy(from_=f"{base_folder}/dst_upload/upload_test_file.txt",
                                  to=f"{base_folder}/dst_copy/upload_test_file.txt",
                                  overwrite=True,
                                  wait=True)
        log_info(f"copy result: {copy_result.status}")

        # move
        move_result = client.move(from_=f"{base_folder}/dst_copy/upload_test_file.txt",
                                  to=f"{base_folder}/dst_move/upload_test_file.txt",
                                  overwrite=True,
                                  wait=True)
        log_info(f"move result: {move_result.status}")

        # mkdir
        mkdir_result = client.mk_dir(source=f"{base_folder}/newDir")
        log_info(f"mkdir result: {mkdir_result}")

        # delete
        delete_result = client.delete(source=f"{base_folder}/newDir")
        log_info(f"delete result: {delete_result}")

        # list
        list_result = client.list(base_folder)
        log_info(f"list result:")
        for result in list_result:
            log_info(f"name: {result.name}, last modified: {result.last_modified}")

    except:
        raise Exception("Something went wrong!!!")
